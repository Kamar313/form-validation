// All Selector
let name2 = document.querySelector("#name");
let email = document.querySelector(`#email`);
let color = document.querySelector("#color");
let option = document.querySelector("#option");
let rateMovie = document.querySelector("#rate");
let allredio = document.querySelectorAll(`input[name='radio']`);
let checkbox = document.querySelector(`input[type='checkbox']`);

let body = document.querySelector("body");

// Main Form listner
document.querySelector(".main-form").addEventListener("submit", (e) => {
  e.preventDefault();
  // check all condition and validation
  let err = false;
  if (name2.value.length < 5) {
    document.querySelector(".nameError").innerHTML = "*Enter your full name";
    err = true;
  } else {
    document.querySelector(".nameError").innerHTML = "";
  }
  if (!email.value.match("@")) {
    document.querySelector(".emailError").innerHTML = "*Enter correct Email";
    err = true;
  } else {
    document.querySelector(".emailError").innerHTML = "";
  }
  let count = 0;
  allredio.forEach((e) => {
    if (!e.checked) {
      count++;
    }
  });

  if (count >= 3) {
    document.querySelector(".redioError").innerHTML = "*Select any one of That";
    err = true;
  } else {
    document.querySelector(".redioError").innerHTML = "";
  }
  if (!checkbox.checked) {
    document.querySelector(".checkboxError").innerHTML = "*Plaese Select T&C";
    err = true;
  } else {
    document.querySelector(".checkboxError").innerHTML = "";
  }

  // Pop Up Form
  if (!err) {
    let popDiv = document.createElement("div");
    popDiv.classList.add("popup");
    popDiv.style.position = "absolute";
    body.append(popDiv);
    let innerDiv = document.createElement("div");
    innerDiv.classList.add("inner-div");
    popDiv.append(innerDiv);
    let closebtn = document.createElement("button");
    closebtn.classList.add("close-btn");
    closebtn.innerText = "Close";
    innerDiv.append(closebtn);
    popDiv.style.backgroundColor = "white";
    popDiv.style.width = "50%";
    popDiv.style.height = "600px";
    popDiv.style.top = "0";
    popDiv.style.left = "25%";
    popDiv.style.display = "flex";
    popDiv.style.justifyContent = "center";
    popDiv.style.alignItems = "center";
    // popDiv.style.backgroundColor = "rgb(75, 240, 240)";

    // popDiv.style.padding = "20%";
    let heading = document.createElement("h1");
    heading.innerText = `Hello ${name2.value}`;
    innerDiv.append(heading);
    let emailHeading = document.createElement("h3");
    innerDiv.append(emailHeading);
    emailHeading.innerText = `Email: ${email.value}`;
    let movieHeading = document.createElement("h3");
    innerDiv.append(movieHeading);
    movieHeading.innerText = `You Love: ${option.value}`;
    let colorHeading = document.createElement("h3");
    innerDiv.append(colorHeading);
    colorHeading.innerText = `Color: ${color.value}`;
    let RateHeading = document.createElement("h3");
    innerDiv.append(RateHeading);
    RateHeading.innerText = `Rating: ${rateMovie.value}`;
    let movieGener = document.createElement("h3");
    innerDiv.append(movieGener);

    allredio.forEach((ele) => {
      if (ele.checked) {
        movieGener.innerText = `Book Gener: ${ele.nextSibling.textContent}`;
      }
    });
    let footer = document.createElement("h4");
    innerDiv.append(footer);
    footer.innerText = "You agreed to Tearms and Condition";
    closebtn.addEventListener("click", () => {
      location.reload();
    });
  }
});
